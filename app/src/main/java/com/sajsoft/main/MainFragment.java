package com.sajsoft.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.sajsoft.R;
import com.sajsoft.databinding.FragmentMainBinding;
import com.sajsoft.home.HomeFragment;


public class MainFragment extends BaseFragment {

    private FragmentMainBinding binding;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        binding = FragmentMainBinding.inflate(inflater, container, false);

        HomePagerAdapter adapter = new HomePagerAdapter(this);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.setUserInputEnabled(false);

        return  binding.getRoot();
    }

    private static class HomePagerAdapter extends FragmentStateAdapter {

        public HomePagerAdapter(@NonNull Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            if (position == 0) {
                return HomeFragment.newInstance();
            } else {
                return null;
            }
        }

        @Override
        public int getItemCount() {
            return 1;
        }
    }

}