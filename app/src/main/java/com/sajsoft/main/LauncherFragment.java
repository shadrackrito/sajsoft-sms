package com.sajsoft.main;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sajsoft.databinding.FragmentLauncherScreenBinding;


public class LauncherFragment extends BaseFragment {

    public LauncherFragment() {
        // Required empty public constructor
    }

    public static LauncherFragment newInstance() {
        return new LauncherFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        com.sajsoft.databinding.FragmentLauncherScreenBinding binding = FragmentLauncherScreenBinding.inflate(inflater, container, false);


        new Handler().postDelayed(() -> navigate(LauncherFragmentDirections.actionLauncherToMain()), 2000);

        return binding.getRoot();
    }


}