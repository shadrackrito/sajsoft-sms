package com.sajsoft.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import javax.inject.Inject;

import com.sajsoft.app.TililtechApplication;
import com.sajsoft.app.api.APIResponse;
import com.sajsoft.app.api.database.models.Account;
import com.sajsoft.app.api.responses.StkPushResponse;
import com.sajsoft.app.repositories.AuthRepository;

import java.util.List;


public class HomeViewModel extends AndroidViewModel {

    @Inject
    AuthRepository authRepository;


    public HomeViewModel(@NonNull Application application) {
        super(application);
        ((TililtechApplication) application).getApplicationComponent().inject(this);
    }


    public LiveData<APIResponse<StkPushResponse>> stkPush(String phoneNumber, String accountNumber, String amount) {
        return authRepository.stkPush(phoneNumber, accountNumber, amount);
    }


}
