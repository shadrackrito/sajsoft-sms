package com.sajsoft.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import com.sajsoft.R;
import com.sajsoft.app.api.Utils.TextValidator;
import com.sajsoft.app.api.database.AppDatabase;
import com.sajsoft.app.api.database.dao.AccountDao;
import com.sajsoft.app.api.database.models.Account;
import com.sajsoft.databinding.FragmentHomeBinding;
import com.sajsoft.main.BaseFragment;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment {


    private FragmentHomeBinding binding;
    private HomeViewModel homeViewModel;

    private List<Account> accountList;

    private AppDatabase appDatabase;

    private ArrayAdapter<Account> accountArrayAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel =new ViewModelProvider(this).get(HomeViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);

        appDatabase = Room.databaseBuilder(requireContext(),
                AppDatabase.class, "myapp-database").build();
        binding.txtAccountNo.addTextChangedListener(new TextValidator() {
            @Override
            public void validate(String s) {
                if (s != null) {
                    binding.tilAccountNo.setError(null);
                }
            }
        });

        binding.txtTelephone.addTextChangedListener(new TextValidator() {
            @Override
            public void validate(String s) {
                if (s != null) {
                    binding.tilTelephone.setError(null);
                }
            }
        });

        binding.txtAmount.addTextChangedListener(new TextValidator() {
            @Override
            public void validate(String s) {
                if (s != null) {
                    binding.tilAmount.setError(null);
                }
            }
        });

        binding.ivWhatsapp.setOnClickListener(v -> {
            openWhatsappHelp();
        });


        binding.spinnerServices.setOnItemClickListener((parent, view, position, id) -> {
            if (accountList != null && accountList.size() > 0) {
                binding.txtAccountNo.setText(accountArrayAdapter.getItem(position).getAccountNo());

            } else {
                Toast.makeText(requireContext(), "No services available", Toast.LENGTH_SHORT).show();
            }
        });

        binding.tvPay.setOnClickListener(view -> {
            String accountNo = binding.txtAccountNo.getText().toString().trim();
            String amount = binding.txtAmount.getText().toString().trim();
            String telephone = binding.txtTelephone.getText().toString().trim();

            if (TextValidator.isEmpty(accountNo)) {
                binding.tilAccountNo.setError("This field is required");
            } else if (TextValidator.isEmpty(telephone)) {
                binding.tilTelephone.setError("This field is required");
            } else if (TextValidator.isEmpty(amount)) {
                binding.tilAmount.setError("This field is required");
            } else {
                if (telephone.length() == 10 && (telephone.startsWith("01") || telephone.startsWith("07"))) {
                   if (Integer.parseInt(amount) > 0){
                       sendStk(telephone, "Vas." + accountNo, amount, accountNo);
                   }else {
                       binding.tilTelephone.setError("Amount cannot be zero (0)");

                   }
                } else {
                    binding.tilTelephone.setError("Invalid phone number. Start with 07... or 01...");
                }
            }


        });

        if (accountList != null && accountList.size() > 0) {
            accountArrayAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, accountList);

            binding.spinnerServices.setAdapter(accountArrayAdapter);

            accountArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        loadDataFromDb();

        return binding.getRoot();

    }

        private void loadDataFromDb() {
            new AsyncTask<Void, Void, List<Account>>() {
                @Override
                protected List<Account> doInBackground(Void... voids) {
                    // Perform database query in background thread
                    return appDatabase.accountDao().getAccountsList();
                }

                @Override
                protected void onPostExecute(List<Account> myDataList) {
                    // Update UI with data from database on main thread
                    updateUiWithData(myDataList);
                }
            }.execute();
        }

    private void updateUiWithData(List<Account> myDataList) {
        accountList = myDataList;
        if (myDataList != null && myDataList.size() > 0){
            accountArrayAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, myDataList);

            binding.spinnerServices.setAdapter(accountArrayAdapter);

            accountArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
    }



    private void insertAccount(Account account) {
        new InsertAccountTask().execute(account);
    }


    private class InsertAccountTask extends AsyncTask<Account, Void, Void> {
        @Override
        protected Void doInBackground(Account... accounts) {
            Account account = accounts[0];

            boolean isInList=false;
            for (Account item1: accountList) {
                if (item1.getAccountNo().equals(account.getAccountNo())){
                    item1.setAccountNo(account.getAccountNo());
                    isInList=true;
                    break;
                }
            }
            if(!isInList) {
                appDatabase.accountDao().insert(account);

            }
            return null;
        }
    }


    @SuppressLint("QueryPermissionsNeeded")
    private void openWhatsappHelp() {

        String number = getString(R.string.whatsapp_help_number);
        String message= getString(R.string.whatsapp_help_text);
        try{
            PackageManager packageManager = requireContext().getPackageManager();
            Intent whatsappIntent = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone="+ number +"&text=" + URLEncoder.encode(message, "UTF-8");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.setData(Uri.parse(url));
            if (whatsappIntent.resolveActivity(packageManager) != null) {
                startActivity(whatsappIntent);
            }else {
                Toast.makeText(requireContext(), R.string.please_install_whatsapp, Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e) {
            Toast.makeText(requireContext(), R.string.please_install_whatsapp, Toast.LENGTH_SHORT).show();
        }
    }


    private void sendStk(String phoneNumber, String accountNumber, String amount, String accountNo) {
        ProgressDialog progressDialog = ProgressDialog.show(requireContext(), "", "Initiating STK Push. Please wait...", true);
        homeViewModel.stkPush(phoneNumber,accountNumber,amount).observe(getViewLifecycleOwner(), apiResponse -> {
            progressDialog.dismiss();
            if (apiResponse!=null && apiResponse.isSuccessful()) {
                successDialog("STK push request initiated. You will receive a confirmation message shortly. Please enter your Mpesa PIN to complete the transaction.");

                Account account = new Account(accountNo);

                insertAccount(account);

                loadDataFromDb();


            }
        });
    }

    private void successDialog(String message){
        SuccessDialogFragment dialogFragment = new SuccessDialogFragment();
        Bundle args= new Bundle();
        args.putInt("type",124);
        args.putString("message", message);
        dialogFragment.setArguments(args);
        dialogFragment.show(getChildFragmentManager(),dialogFragment.getTag());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 124 && resultCode == Activity.RESULT_OK && data != null) {

            binding.txtAmount.setText(null);
            binding.txtTelephone.setText(null);
            binding.txtAccountNo.setText(null);
            binding.txtAccountNo.requestFocus();
        }
    }



}