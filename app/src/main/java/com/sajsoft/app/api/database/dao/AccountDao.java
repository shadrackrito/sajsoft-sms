package com.sajsoft.app.api.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.sajsoft.app.api.database.models.Account;

import java.util.List;

@Dao
public interface AccountDao {

    @Query("SELECT * FROM accounts ORDER BY id DESC ")
    List<Account> getAccountsList();

    @Query("SELECT * FROM accounts  where id=:id ORDER BY id DESC ")
    Account getAccountsListById(String id);

    @Insert
    void insert(Account account);
}