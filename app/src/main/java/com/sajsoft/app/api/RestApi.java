package com.sajsoft.app.api;



import com.sajsoft.app.api.requests.StkPushRequest;
import com.sajsoft.app.api.responses.StkPushResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RestApi {

    @POST("v1/hlab/stk/tilil_push")
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    Call<StkPushResponse> stkPush(@Body StkPushRequest request);

}