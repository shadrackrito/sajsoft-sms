package com.sajsoft.app.api.database.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

@Entity(tableName = "accounts")
public class Account implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int id;

    public Account(String accountNo) {
        this.accountNo = accountNo;
    }

    @ColumnInfo(name = "account_no")
    String accountNo;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id && Objects.equals(accountNo, account.accountNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNo);
    }

    @NonNull
    @Override
    public String toString() {
        return accountNo+"";
    }
}