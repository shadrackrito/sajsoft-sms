package com.sajsoft.app.api.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.sajsoft.app.api.database.dao.AccountDao;
import com.sajsoft.app.api.database.models.Account;

@Database(entities = {Account.class}, version = 2, exportSchema = false)
public  abstract class AppDatabase extends RoomDatabase {

    public abstract AccountDao accountDao();

}