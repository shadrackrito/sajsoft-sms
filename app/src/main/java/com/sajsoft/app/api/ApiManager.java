package com.sajsoft.app.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.sajsoft.app.api.requests.StkPushRequest;
import com.sajsoft.app.api.responses.StkPushResponse;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class ApiManager {
    private static final int READ_TIMEOUT = 60;
    private static final int CONNECT_TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 60;
    public static final String SERVER_URL = "http://66.45.225.230/ubergas_portal/public/";
    private static final int NUMBER_OF_THREADS = 6;
    private static final ExecutorService apiExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private final RestApi api;

    @Inject
    public ApiManager() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(SERVER_URL)
                .client(client)
                .build();

        api = retrofit.create(RestApi.class);
    }

    public static void execute(Runnable runnable) {
        apiExecutor.execute(runnable);
    }



    public APIResponse<StkPushResponse> stkPush(String phoneNumber, String accountNumber, String amount) throws IOException {
        StkPushRequest request=new StkPushRequest();
        request.phoneNumber=phoneNumber;
        request.accountNumber=accountNumber;
        request.amount=amount;
        return new APIResponse<>(api.stkPush(request).execute());
    }



}