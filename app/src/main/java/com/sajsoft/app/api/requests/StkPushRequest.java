package com.sajsoft.app.api.requests;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class StkPushRequest implements Serializable {

    @SerializedName("phone_number")
    public String phoneNumber;
    @SerializedName("amount")
    public String amount;
    @SerializedName("account_number")
    public String accountNumber;


}
