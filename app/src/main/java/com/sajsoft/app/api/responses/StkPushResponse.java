package com.sajsoft.app.api.responses;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class StkPushResponse implements Serializable {

    @SerializedName("MerchantRequestID")
    public String MerchantRequestID;
    @SerializedName("CheckoutRequestID")
    public String CheckoutRequestID;
    @SerializedName("ResponseCode")
    public String ResponseCode;
    @SerializedName("ResponseDescription")
    public String ResponseDescription;
    @SerializedName("CustomerMessage")
    public String CustomerMessage;

}
