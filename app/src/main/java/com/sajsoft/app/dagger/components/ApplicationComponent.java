package com.sajsoft.app.dagger.components;


import javax.inject.Singleton;

import com.sajsoft.app.dagger.modules.ApplicationModule;
import com.sajsoft.home.HomeViewModel;
import dagger.Component;


@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {


    void inject(HomeViewModel homeViewModel);

}