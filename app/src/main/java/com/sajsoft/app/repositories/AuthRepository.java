package com.sajsoft.app.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.sajsoft.app.api.APIResponse;
import com.sajsoft.app.api.ApiManager;
import com.sajsoft.app.api.responses.StkPushResponse;


@Singleton
public class AuthRepository {
    private final ApiManager apiManager;
    private final Application mContext;


    @Inject
    public AuthRepository(ApiManager apiManager, Application context) {
        this.apiManager = apiManager;
        this.mContext = context;
    }

    public LiveData<APIResponse<StkPushResponse>> stkPush(String phoneNumber, String accountNumber, String amount) {
        MutableLiveData<APIResponse<StkPushResponse>> liveData = new MutableLiveData<>();
        ApiManager.execute(() -> {
            try {
                APIResponse<StkPushResponse> response = apiManager.stkPush(phoneNumber, accountNumber, amount);
                liveData.postValue(response);
            } catch (IOException e) {
                e.printStackTrace();
                liveData.postValue(null);
            }
        });
        return liveData;
    }


}

