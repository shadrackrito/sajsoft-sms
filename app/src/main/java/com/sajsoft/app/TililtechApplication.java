package com.sajsoft.app;

import android.app.Application;

import com.sajsoft.app.dagger.components.ApplicationComponent;
import com.sajsoft.app.dagger.components.DaggerApplicationComponent;
import com.sajsoft.app.dagger.modules.ApplicationModule;


public class TililtechApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent =  DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
