package com.sajsoft.app.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Wallet implements Serializable {

    @SerializedName("account_number")
    public String account_number;
    @SerializedName("account_type")
    public String account_type;
    @SerializedName("credit_balance")
    public float credit_balance;
    @SerializedName("post_paid")
    public String post_paid;
    @SerializedName("default_sms_rate")
    public float default_sms_rate;
}
