package com.sajsoft;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.sajsoft.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}